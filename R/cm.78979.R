#' Produção em função de níveis de calcáreo.
#'
#' Um data.frame contendo a produção de soja (kg/ha) em função da dose da calcareo (ton/ha) em Campo Mourão no ano de 1979/80.
#'
#'
#' @format Um data.frame com 24 observações de 3 variáveis:
#' \describe{
#'   \item{calc}{dose de calcáreo, ton/ha}
#'   \item{bloco}{bloco}
#'   \item{prod}{produção de soja, kg/ha}
#' }

"cm.7980"
