


# 1a. aproximação de Mitscherlich
#       a * (1 - 10 ^ (-c * (x + b)))
mit1 <-
    function(x,a,b,c) {
        a * (1 - 10 ^ (-c * (x + b)))
    }

# 2a. aproximação de Mitscherlich
#       a * (1 - 10 ^ (-c * (x + b))) * 10 ^ (-k * (x + b) ^ 2)
mit2 <-
    function(x,a,b,c,k) {
        a * (1 - 10 ^ (-c * (x + b))) * 10 ^ (-k * (x + b) ^ 2)
    }

